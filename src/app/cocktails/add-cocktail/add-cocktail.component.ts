import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Cocktail } from '../../shared/cocktail.model';
import { CocktailService } from '../../shared/cocktail.service';
import { ActivatedRoute, Router } from '@angular/router';
import { urlValidator } from '../../validate-url.directive';

@Component({
  selector: 'app-add-cocktail',
  templateUrl: './add-cocktail.component.html',
  styleUrls: ['./add-cocktail.component.css']
})
export class AddCocktailComponent implements OnInit {
  cocktailForm!: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private cocktailService: CocktailService,
  ) { }

  save() {
    console.log(this.cocktailForm);
  }

  ngOnInit(): void {
    this.cocktailForm = new FormGroup({
      name: new FormControl('', Validators.required),
      imageCocktail: new FormControl('', [Validators.required, urlValidator]),
      type: new FormControl('', Validators.required),
      description: new FormControl(''),
      ingredients: new FormArray([]),
      instructions: new FormControl('', Validators.required)
    })
  }

  addIngredient() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const ingredientGroup = new FormGroup({
      ingredientName: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      measurability: new FormControl('', Validators.required)
    });
    ingredients.push(ingredientGroup);
  }

  getIngredientControls() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    return ingredients.controls;
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.cocktailForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  addNewCocktail() {
    const id = Math.random().toString();
    const cocktail = new Cocktail(
      id,
      this.cocktailForm.value.description,
      this.cocktailForm.value.imageCocktail,
      this.cocktailForm.value.ingredients,
      this.cocktailForm.value.instructions,
      this.cocktailForm.value.name,
      this.cocktailForm.value.type,
    );

    this.cocktailService.addCocktail(cocktail).subscribe(() => {
      this.cocktailService.fetchCocktails();
      void this.router.navigate(['..'], {relativeTo: this.route});
    });
  }

  removeIngredient(id: any) {
    const field = <FormArray>this.cocktailForm.get('ingredients');
    field.removeAt(id);
  }

}
