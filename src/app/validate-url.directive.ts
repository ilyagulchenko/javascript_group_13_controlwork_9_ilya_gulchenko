import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

export const urlValidator = (control: AbstractControl): ValidationErrors | null => {
  const format = /^((http|https):\/\/)(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)/i.test(control.value);

  if(format) {
    return null;
  }
  return {url: true};
};

@Directive({
  selector: '[appUrl]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateUrlDirective,
    multi: true
  }]
})

export class ValidateUrlDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return urlValidator(control);
  }
}
