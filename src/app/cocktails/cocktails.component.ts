import { Component, OnDestroy, OnInit } from '@angular/core';
import { Cocktail } from '../shared/cocktail.model';
import { Subscription } from 'rxjs';
import { CocktailService } from '../shared/cocktail.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.css']
})
export class CocktailsComponent implements OnInit, OnDestroy {
  cocktails!: Cocktail[];
  cocktailsChangeSubscription!: Subscription;

  constructor(
    private router: Router,
    private cocktailService: CocktailService,
  ) { }

  ngOnInit(): void {
    this.cocktailsChangeSubscription = this.cocktailService.cocktailsFetch.subscribe((cocktails: Cocktail[]) => {
      this.cocktails = cocktails;
    });
    this.cocktailService.fetchCocktails();
  }

  ngOnDestroy(): void {
    this.cocktailsChangeSubscription.unsubscribe();
  }

}
