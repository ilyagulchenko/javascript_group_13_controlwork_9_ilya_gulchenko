export class Cocktail {
  constructor(
    public id: string,
    public description: string,
    public imageCocktail: string,
    public ingredients: any,
    public instructions: string,
    public name: string,
    public type: string,
  ) {}
}
