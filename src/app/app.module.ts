import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CocktailsComponent } from './cocktails/cocktails.component';
import { CocktailItemComponent } from './cocktails/cocktail-item/cocktail-item.component';
import { AddCocktailComponent } from './cocktails/add-cocktail/add-cocktail.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CocktailService } from './shared/cocktail.service';
import { HttpClientModule } from '@angular/common/http';
import { ValidateUrlDirective } from './validate-url.directive';

@NgModule({
  declarations: [
    AppComponent,
    CocktailsComponent,
    CocktailItemComponent,
    AddCocktailComponent,
    ToolbarComponent,
    ValidateUrlDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [CocktailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
