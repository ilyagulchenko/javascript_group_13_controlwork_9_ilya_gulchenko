import { Injectable } from '@angular/core';
import { Cocktail } from './cocktail.model';
import { HttpClient } from '@angular/common/http';
import { map, Subject } from 'rxjs';

@Injectable()

export class CocktailService {
  cocktailsFetch = new Subject<Cocktail[]>();

  constructor(private http: HttpClient) {}

  private cocktails: Cocktail[] = [];

  addCocktail(cocktail: Cocktail) {
    const body = {
      description: cocktail.description,
      imageCocktail: cocktail.imageCocktail,
      ingredients: cocktail.ingredients,
      instructions: cocktail.instructions,
      name: cocktail.name,
      type: cocktail.type,
    };

    return this.http.post('https://plovo-cc061-default-rtdb.firebaseio.com/cocktails.json', body);
  }

  fetchCocktails() {
    this.http.get<{[id: string]: Cocktail}>('https://plovo-cc061-default-rtdb.firebaseio.com/cocktails.json').pipe(
      map(result => {
        return Object.keys(result).map(id => {
          const cocktailData = result[id];

          return new Cocktail(
            id,
            cocktailData.description,
            cocktailData.imageCocktail,
            cocktailData.ingredients,
            cocktailData.instructions,
            cocktailData.name,
            cocktailData.type,
          );
        });
      })
    ).subscribe(recipes => {
      this.cocktails = recipes;
      this.cocktailsFetch.next(this.cocktails.slice());
    })
  }
}
